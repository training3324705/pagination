import { useState } from "react";
import HomePage from "pages/homepage";
import Users from "pages/users";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import UserManagement from "pages/usermanagement";
import ProtectedRoutes from "components/proectedroute";
import { UserContext } from "services/context";

function App() {
  const [userDetails, setUserDetails] = useState();
  console.log(userDetails);

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ userDetails }}>
        <Routes>
          <Route
            exact
            path="/usersmanagement"
            element={
              <UserManagement
                callBackFun={(data) => {
                  setUserDetails(data);
                }}
              />
            }
          />
          <Route element={<ProtectedRoutes />}>
            <Route exact path="/" element={<HomePage />} />
            <Route exact path="/users" element={<Users />} />
          </Route>
        </Routes>
      </UserContext.Provider>
    </BrowserRouter>
  );
}

export default App;
