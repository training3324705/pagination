import styled from "styled-components";

export const MainWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #F0F8FF;
`;

export const LoginCardWrapper = styled.div`
  width: 40vw;
  height: 50vh;
  border: 2px solid grey;
  background-color: #F5F5DC;
`;

export const LoginHeadingWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  
`;
export const LoginHeading = styled.h1`
    color: lightsteelblue;
`;

export const SeparationLine = styled.div`
    width: 40vw;
    height: 0.2vh;
   background-color: grey;
`;

export const Form = styled.form`
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
`

export const UserInput = styled.input`
    width: 25vw;
    height: 5vh;
    margin-top: 2vh;
`
export const Errors = styled.p`
   color: red;
`

export const LoginButtn = styled.button`
    width: 26vw;
    height: 5vh;
    font-weight:800;
    margin-top: 2vh;
    background-color: lightsteelblue;
    border: none;

    :hover{
        cursor: pointer;
        background-color: lightseagreen;
        color: white;
    }
`