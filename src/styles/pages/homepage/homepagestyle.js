import styled from "styled-components";

export const Wrapper = styled.div`
display:flex;
flex-direction: column;
align-items: center;
justify-content: center;
`
export const UserDetailsWrapper = styled.div`
    background-color: whitesmoke;
    border: 2px solid grey;
    margin-top: 2vh;
    text-align: center;
    width: 40vw;
    height: 16vh;
`
export const Heading = styled.h1`
    color: green;
    text-shadow: 3px 3px #888888;
    font-size: 2vw;
`
export const SeparationLine = styled.div`
    width: 40vw;
    height: 0.2vh;
   background-color: grey;
`;
export const UserName = styled.div`
    color: blue;
    font-size: 2vw;
`

export const UserPassword = styled.div`
    color: blue;
    font-size: 2vw;
`


export const LogOutBtn = styled.button`
    margin-top: 2vh;
    color: white;
    font-size: 1.5vw;
    font-weight: 800;
    border: none;
    width: 7vw;
    height: 5vh;
    background-color: green;
    cursor: pointer;

    :hover{
        color: black;
        background-color: aqua;
        border: 2px solid black;
    }
`