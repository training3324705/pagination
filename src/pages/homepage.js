import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "services/context";
import {
  Wrapper,
  Heading,
  LogOutBtn,
  UserDetailsWrapper,
  SeparationLine,
  UserName,
  UserPassword,
} from "styles/pages/homepage/homepagestyle";

const HomePage = ()=> {
  const { userDetails } = useContext(UserContext);
  console.log(userDetails, "dehioehioehwfh");
  const navigate = useNavigate();
  const handleClick = () => {
    localStorage.removeItem("userToken");
    navigate("/usersmanagement");
  };

  return (
    <Wrapper>
      <UserDetailsWrapper>
        <Heading>User Profile</Heading>
        <SeparationLine />
        <UserName>Name: {userDetails.username}</UserName>
        <UserPassword>Password: {userDetails.password}</UserPassword>
      </UserDetailsWrapper>
      <LogOutBtn onClick={handleClick}>Logout</LogOutBtn>
    </Wrapper>
  );
}

export default HomePage;
