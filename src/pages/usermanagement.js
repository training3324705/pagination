import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import {
  MainWrapper,
  LoginCardWrapper,
  LoginHeading,
  LoginHeadingWrapper,
  SeparationLine,
  Form,
  UserInput,
  LoginButtn,
  Errors,
} from "styles/pages/usermanagement/usermanstyle";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
// import forlogin from "../services/forlogin"

const schema = yup
  .object({
    Name: yup.string().required(),
    Password: yup.string().required("Please Enter your password"),
    // .matches(
    //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    //   "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    //   ),
  })
  .required();

const UserManagement = ({ callBackFun }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const navigate = useNavigate();
  
  useEffect(() => {
    if (localStorage.getItem("userToken")) navigate("/");
  });

  const onSubmit = ({ Name, Password }) => {
    var data = {
      username: Name,
      password: Password,
    };
    // console.log("name",Name);
    // console.log("pass",Password);
    console.log(data);

    axios({
      url: "https://fakestore api.com/auth/login",
      method: "POST",
      data
      // data: {
      //   username: Name,
      //   password: Password,
      // },
      
    })
      .then((res) => {
        console.log(res.data.token);
        localStorage.setItem("userToken", res.data.token);
        !"userToken" ? navigate("/usersmanagement") : navigate("/");
      })
      .catch((err) => {
        console.log(err.response);
      });
    callBackFun(data);
  };
  return (
    <MainWrapper>
      <LoginCardWrapper>
        <LoginHeadingWrapper>
          <LoginHeading>Login</LoginHeading>
        </LoginHeadingWrapper>
        <SeparationLine />

        <Form onSubmit={handleSubmit(onSubmit)}>
          <UserInput placeholder="UserName" {...register("Name")} />
          <Errors>{errors.Name?.message}</Errors>
          <UserInput placeholder="UserPassword" {...register("Password")} />
          <Errors>{errors.Password?.message}</Errors>
          <LoginButtn type="submit">Login</LoginButtn>
        </Form>
      </LoginCardWrapper>
    </MainWrapper>
  );
};

export default UserManagement;
